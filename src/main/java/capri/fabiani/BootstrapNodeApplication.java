package capri.fabiani;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import capri.fabiani.aws.SnsManager;

@SpringBootApplication
public class BootstrapNodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootstrapNodeApplication.class, args);
		SnsManager.subscribe();
	}
}
