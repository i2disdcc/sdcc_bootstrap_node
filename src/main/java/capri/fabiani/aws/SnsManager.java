package capri.fabiani.aws;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import com.amazonaws.Response;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.SubscribeRequest;

import capri.fabiani.model.rest.DTOSNS;
import capri.fabiani.util.Costants;

public class SnsManager {

	
	public static void subscribe(){
		AmazonSNSClient snsClient = new AmazonSNSClient(new ClasspathPropertiesFileCredentialsProvider());		                           
		snsClient.setRegion(Region.getRegion(Regions.US_EAST_1));
		String localhost="127.0.0.1";
		try {
			localhost = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String address= Costants.AWS_SNS_ENDPOINT_URL.replace("IP_REPLICA", localhost);
		SubscribeRequest subRequest = new SubscribeRequest(Costants.ARN, "http", address);
		snsClient.subscribe(subRequest);		
	}
	
	public static void processMessage(DTOSNS dtoSNS){
		//Get the message type header.
				String messagetype = dtoSNS.getType();
				//If message doesn't have the message type header, don't process it.
				if (messagetype == null)
					return;

		 
		        
		    // The signature is based on SignatureVersion 1. 
		    // If the sig version is something other than 1, 
		    // throw an exception.
		    if (dtoSNS.getSignatureVersion().equals("1")) {
		    	throw new SecurityException("Unexpected signature version. Unable to verify signature.");
		    }
		    else {
				/*TODO verify signature*/
		      
		    }

		    // Process the message based on type.
				if (messagetype.equals(Costants.AWS_SNS_MESSAGE_MESSAGE)) {
				System.out.println(dtoSNS.toString());
				
				}
		    else if (messagetype.equals(Costants.AWS_SNS_MESSAGE_SUSCRIBE))
				{
		    		//TODO: We should make sure that this subscription is from the topic you expect. Compare topicARN to our topic (topics) 
		    	//TODO: Do something with the Message and Subject.
				HttpResponse response = null;
				HttpClient client = HttpClientBuilder.create().build();
				HttpGet request = new HttpGet(dtoSNS.getSubscribeURL());
				try {
					response = client.execute(request);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;
				}
				if(response!=null){
					if(response.getStatusLine().getStatusCode()== HttpStatus.SC_OK){
						//tutto bene
						
					}
					else{
						//errore non sono iscritto al topic
						
					}
				}
				else{
						//non dovrebbe entrarci a causa del catch
				}
				}
		    else if (messagetype.equals(Costants.AWS_SNS_MESSAGE_UNSUSCRIBE)) {
		      //TODO: La cancellazione della sottoscrizione non viene ancora gestita, ci serve?
				}
		    else {
		      //TODO: Handle unknown message type.
		    }
	}
}
