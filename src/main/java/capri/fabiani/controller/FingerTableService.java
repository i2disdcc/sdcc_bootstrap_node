package capri.fabiani.controller;

import org.springframework.http.ResponseEntity;

import capri.fabiani.model.rest.DTONode;
import capri.fabiani.model.rest.DTOresponse;

public interface FingerTableService {

	ResponseEntity<DTONode> getNewId(String ip);
	ResponseEntity<DTONode> getBeforeAfter(long id);
	
}
