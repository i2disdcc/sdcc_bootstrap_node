package capri.fabiani.controller.implementation;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import capri.fabiani.controller.FingerTableService;
import capri.fabiani.model.FingerTable;
import capri.fabiani.model.rest.DTONode;
import capri.fabiani.model.rest.DTOresponse;
import capri.fabiani.util.IpIdAssociation;

@Service("FingerTableService")
public class FingerTableServiceImpl implements FingerTableService {

	FingerTable ft;
	long id;

	@Override
	public ResponseEntity<DTONode> getNewId(String ip) {
		checkNewFinger();
		//TODO serve una lista di richieste? bisogna controllare lo stato di cloudwatch?
		//serve sapere come implementare i servizi amazon
		ft.insert(new IpIdAssociation(ip,++id));
		return new ResponseEntity<DTONode>(new DTONode(id), HttpStatus.OK);

	}

	@Override
	public ResponseEntity<DTONode> getBeforeAfter(long id) {
		checkNewFinger();
		IpIdAssociation[] result = ft.getBeforeAfter(id);
		if (result != null)
			return new ResponseEntity<DTONode>(new DTONode(result), HttpStatus.OK);
		else
			return new ResponseEntity<DTONode>(
					new DTONode("Id non trovato",
							"L'id richiesto non è nella finger table la richiesta non può essere soddisfatta"),
					HttpStatus.NOT_FOUND);

	}

	private void checkNewFinger() {
		if (ft == null) {
			ft = new FingerTable();
		}
	}

}
