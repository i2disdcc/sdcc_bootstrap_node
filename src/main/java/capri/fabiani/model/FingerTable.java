package capri.fabiani.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import capri.fabiani.util.IpIdAssociation;

@Document
public class FingerTable {

	@Id
	private String id;
	
	private List<IpIdAssociation> table;
	
	public FingerTable() {
		table = new ArrayList<IpIdAssociation>();
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<IpIdAssociation> getTable() {
		return table;
	}

	public void setTable(List<IpIdAssociation> table) {
		this.table = table;
	}

	/**
	 * @param id
	 * @return L'ip associato all'id se presente, una null altrimenti
	 */
	public String getIpFromId(Long id) {
		String result = null;
		int position = binarySearch(id, 0, table.size() - 1);
		if (position >= 0 && position <= table.size() - 1) {
			result = table.get(binarySearch(id, 0, table.size() - 1)).getIp();
			// for (IpIdAssociation ipIdAssociation : table) {
			// if(ipIdAssociation.getId()==id){
			// result=ipIdAssociation.getIp();
			// break;
			// }
			// }
		}
		return result;
	}

	/**
	 * @param ip
	 * @return L'id associato all'ip se presente, null altrimenti
	 */
	public Long getIdFromIp(String ip) {
		Long result = null;
		for (IpIdAssociation ipIdAssociation : table) {
			if (ipIdAssociation.getIp().equals(ip)) {
				result = ipIdAssociation.getId();
				break;

			}
		}
		return result;
	}

	public void insert(IpIdAssociation iia) {
		// loop through all elements
		for (int i = 0; i < table.size() - 1; i++) {
			// if the element you are looking at is smaller than x,
			// go to the next element
			if (table.get(i).getId() < iia.getId())
				continue;
			// if the element equals x, return, because we don't add duplicates
			if (table.get(i).getId() == iia.getId()) {
				table.remove(i);
				table.add(i, iia);
				return;
			}
			// otherwise, we have found the location to add x
			table.add(i, iia);
			return;
		}
		// we looked through all of the elements, and they were all
		// smaller than x, so we add ax to the end of the list
		table.add(iia);
	}

	private Integer binarySearch(long key, int imin, int imax) {
		// test if array is empty
		if (imax < imin)
			// set is empty, so return value showing not found
			return -1;
		else {
			// calculate midpoint to cut set in half
			int imid = midpoint(imin, imax);

			// three-way comparison
			if (table.get(imid).getId() > key)
				// key is in lower subset
				return binarySearch(key, imin, imid - 1);
			else if (table.get(imid).getId() < key)
				// key is in upper subset
				return binarySearch(key, imid + 1, imax);
			else
				// key has been found
				return imid;
		}
	}

	private int midpoint(int imin, int imax) {

		return (imin + imax) / 2;
	}

	
	/**
	 * @param id
	 * @return le voci relative al nodo precedente e successivo presenti nella finger
	 *         table, se l'id richiesto è presente nella finger table, null altrimenti.
	 */
	public IpIdAssociation[] getBeforeAfter(long id) {
		IpIdAssociation[] iia = new IpIdAssociation[2];
		// considero l'id positivo, molto probabilmente dovremo utilizzare
		// unsigned long
		Integer position = binarySearch(id, 0, table.size() - 1);
		if (position != null) {
			iia[0] = table.get((position - 1) % table.size() - 1);
			iia[1] = table.get((position + 1) % table.size() - 1);
			return iia;
		} else {
			// TODO inserire l'errore perchè non ha trovato alcun id nella
			// fingher table

			return null;
		}
	}

}
