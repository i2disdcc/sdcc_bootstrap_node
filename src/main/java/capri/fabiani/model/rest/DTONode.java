package capri.fabiani.model.rest;

import capri.fabiani.util.IpIdAssociation;

public class DTONode extends DTO {

	/**
	 * dto : - text title user
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String ip;
	private IpIdAssociation before;
	private IpIdAssociation after;
	
	public DTONode(IpIdAssociation[] beforeAfter) {
		if(beforeAfter[0]!=null)
			after=beforeAfter[0];
		if(beforeAfter[1]!=null)
			before=beforeAfter[1];
	}
	
	public DTONode(long id){
		this.id = id;
	}
	
	public DTONode(String error, String message) {
		this.error=error;
		this.message=message;
	}


	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public IpIdAssociation getBefore() {
		return before;
	}
	
	public void setBefore(IpIdAssociation before) {
		this.before = before;
	}
	public IpIdAssociation getAfter() {
		return after;
	}
	
	public void setAfter(IpIdAssociation after) {
		this.after = after;
	}
	
		
}
