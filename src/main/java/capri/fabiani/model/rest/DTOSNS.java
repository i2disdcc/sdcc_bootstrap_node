package capri.fabiani.model.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class DTOSNS extends DTO {
	
	private static final long serialVersionUID = 1L;
	
	@JsonInclude(Include.NON_NULL)
	private String Type;
	@JsonInclude(Include.NON_NULL)
	private String MessageId;
	@JsonInclude(Include.NON_NULL)
	private String Token;
	@JsonInclude(Include.NON_NULL)
	private String TopicArn;
	@JsonInclude(Include.NON_NULL)
	private String Message;
	@JsonInclude(Include.NON_NULL)
	private String SubscribeURL;
	@JsonInclude(Include.NON_NULL)
	private String Timestamp;
	@JsonInclude(Include.NON_NULL)
	private String SignatureVersion;
	@JsonInclude(Include.NON_NULL)
	private String Signature;
	@JsonInclude(Include.NON_NULL)
	private String SigningCertURL;
	@JsonInclude(Include.NON_NULL)
	private String Subject;
	@JsonInclude(Include.NON_NULL)
	private String UnsubscribeURL;
	
	public String getType() {
		return Type;
	}
	
	public void setType(String type) {
		Type = type;
	}
	
	public String getMessageId() {
		return MessageId;
	}
	
	public void setMessageId(String messageId) {
		MessageId = messageId;
	}
	
	public String getToken() {
		return Token;
	}
	
	public void setToken(String token) {
		Token = token;
	}
	
	public String getTopicArn() {
		return TopicArn;
	}
	
	public void setTopicArn(String topicArn) {
		TopicArn = topicArn;
	}
	
	public String getMessage() {
		return Message;
	}
	
	public void setMessage(String message) {
		Message = message;
	}
	
	public String getSubscribeURL() {
		return SubscribeURL;
	}
	
	public void setSubscribeURL(String subscribeURL) {
		SubscribeURL = subscribeURL;
	}
	
	public String getTimestamp() {
		return Timestamp;
	}
	
	public void setTimestamp(String timestamp) {
		Timestamp = timestamp;
	}
	
	public String getSignatureVersion() {
		return SignatureVersion;
	}
	
	public void setSignatureVersion(String signatureVersion) {
		SignatureVersion = signatureVersion;
	}
	
	public String getSignature() {
		return Signature;
	}
	
	public void setSignature(String signature) {
		Signature = signature;
	}
	
	public String getSigningCertURL() {
		return SigningCertURL;
	}
	
	public void setSigningCertURL(String signingCertURL) {
		SigningCertURL = signingCertURL;
	}

	public String getSubject() {
		return Subject;
	}

	public void setSubject(String subject) {
		Subject = subject;
	}

	public String getUnsubscribeURL() {
		return UnsubscribeURL;
	}

	public void setUnsubscribeURL(String unsubscribeURL) {
		UnsubscribeURL = unsubscribeURL;
	}
	
}
