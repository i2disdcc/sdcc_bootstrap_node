package capri.fabiani.model.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class DTOresponse extends DTO {

	public DTOresponse(DTO dto, String string) {
			
		this.error = dto.getError();
		this.message = string;
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonInclude(Include.NON_NULL)
	String newsid;
	
	
}
