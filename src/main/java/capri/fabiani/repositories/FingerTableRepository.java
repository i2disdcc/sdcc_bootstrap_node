package capri.fabiani.repositories;

import org.springframework.data.repository.CrudRepository;

import capri.fabiani.model.FingerTable;



public interface FingerTableRepository  extends CrudRepository<FingerTable, String> {
	   

	}
