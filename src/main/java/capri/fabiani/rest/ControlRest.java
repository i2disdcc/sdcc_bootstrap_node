package capri.fabiani.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import capri.fabiani.aws.SnsManager;
import capri.fabiani.controller.FingerTableService;
import capri.fabiani.model.rest.DTONode;
import capri.fabiani.model.rest.DTOSNS;
import capri.fabiani.util.Costants;

@RestController
@RequestMapping("/")
public class ControlRest {

	@Autowired
	private FingerTableService fingerTableService;
	
	@RequestMapping(value = "/getId/", method = RequestMethod.GET)
	public ResponseEntity<DTONode> getNewId(){
		  String remoteAddress = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
		return fingerTableService.getNewId(remoteAddress);
	}

	@RequestMapping(value = "/getBeforeAfter/", method = RequestMethod.GET)
	public ResponseEntity<DTONode> getBeforeAfter(@RequestParam("id") String id){
		return fingerTableService.getBeforeAfter(Long.parseLong(id));
	}
	
	@RequestMapping(value = "/"+Costants.AWS_SNS_CONTROLLER+"/", method = RequestMethod.POST)
	public void handleSNSMessage(@RequestBody DTOSNS dtoSNS){
		  SnsManager.processMessage(dtoSNS);
	}
}
