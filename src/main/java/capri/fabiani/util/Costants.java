package capri.fabiani.util;

public class Costants {
//	Aws SNS services
	public final static String ARN="arn:capri.fabiani.aws:sns:us-west-2:678962425877:AlarmOnIstance";
	public final static String AWS_SNS_CONTROLLER = "SNSController";
	public final static String AWS_SNS_ENDPOINT_URL= "http://IP_REPLICA:8080/" + AWS_SNS_CONTROLLER ;
	public final static String AWS_SNS_MESSAGE_SUSCRIBE= "SubscriptionConfirmation";
	public final static String AWS_SNS_MESSAGE_UNSUSCRIBE= "UnsubscribeConfirmation";
	public final static String AWS_SNS_MESSAGE_MESSAGE= "Notification";
	public final static String AWS_SNS_SIGNATURE_VERSION = "1";
	
	
}
