package capri.fabiani.util;

public class IpIdAssociation {
	private Long id;
	private String ip;
		
	public IpIdAssociation(String ip, Long id) {
		super();
		this.id = id;
		this.ip = ip;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	
	
}