package capri.fabiani.util;

import org.springframework.http.ResponseEntity;

import capri.fabiani.model.rest.DTOresponse;

public class ResponseException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ResponseEntity<DTOresponse> re;
	private String message;
	
	public ResponseException(ResponseEntity<DTOresponse> re,String message){
		
		this.setRe(re);
		this.setMessage(message);
		
	}

	public ResponseEntity<DTOresponse> getRe() {
		return re;
	}

	public void setRe(ResponseEntity<DTOresponse> re) {
		this.re = re;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
