package tesi.corrieri.ponza;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;

import capri.fabiani.BootstrapNodeApplication;

import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BootstrapNodeApplication.class)
@WebAppConfiguration
public class NewsprojectApplicationTests {

	@Test
	public void contextLoads() {
	}

}
